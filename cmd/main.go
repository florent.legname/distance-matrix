package main

import (
	"distance-matrix/internal/input"
	"distance-matrix/internal/request"
	"encoding/csv"
	"flag"
	"fmt"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

var inputFileName *string
var apiKey *string

func readFlags() {
	inputFileName = flag.String("inputFileName", "", "The input file name, as a path")
	apiKey = flag.String("apiKey", "", "The Maps API authentication key")

	flag.Parse()
}

func main() {
	readFlags()

	start := time.Now()

	csvInput := input.CSV{
		FileName:               *inputFileName,
		DestinationsLineOffset: 0,
		DestinationsColOffset:  6,
		OriginsLineOffset:      3,
		OriginsAddressOffset:   5,
	}
	csvInput.PrepareData()

	r := request.Request{BatchSize: 25, APIKey: *apiKey}
	results := r.Exec(csvInput)

	for _, result := range results {
		for i := 0; i <= result.Limit-result.Offset; i++ {
			// Update Dest 1 distance from current record
			csvInput.Records[result.Offset+i][6] = fmt.Sprint(result.Rows[i]["elements"][0].Distance["text"])
			// Update Dest 1 time from current record
			csvInput.Records[result.Offset+i][7] = fmt.Sprint(result.Rows[i]["elements"][0].Duration["text"])

			// Update Dest 2 distance from current record
			csvInput.Records[result.Offset+i][8] = fmt.Sprint(result.Rows[i]["elements"][1].Distance["text"])
			// Update Dest 2 time from current record
			csvInput.Records[result.Offset+i][9] = fmt.Sprint(result.Rows[i]["elements"][1].Duration["text"])

			// Update Dest 3 distance from current record
			csvInput.Records[result.Offset+i][10] = fmt.Sprint(result.Rows[i]["elements"][2].Distance["text"])
			// Update Dest 3 time from current record
			csvInput.Records[result.Offset+i][11] = fmt.Sprint(result.Rows[i]["elements"][2].Duration["text"])
		}
	}

	csvFile, err := os.OpenFile(*inputFileName, os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	w := csv.NewWriter(csvFile)
	err = w.WriteAll(csvInput.Records)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	log.Info().Str("ExecutionTime", fmt.Sprintf("%v", time.Since(start))).Send()
}
