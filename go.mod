module distance-matrix

go 1.19

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/rs/zerolog v1.28.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
