package request

import (
	"bufio"
	"distance-matrix/internal/input"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"reflect"
	"strings"
	"testing"
)

func origins(t testing.TB) []input.Origin {
	t.Helper()

	return []input.Origin{
		{Index: 1, Address: "Address1"},
		{Index: 2, Address: "Address2"},
		{Index: 3, Address: "Address3"},
		{Index: 4, Address: "Address4"},
		{Index: 5, Address: "Address5"},
		{Index: 6, Address: "Address6"},
		{Index: 7, Address: "Address7"},
		{Index: 8, Address: "Address8"},
		{Index: 9, Address: "Address9"},
		{Index: 10, Address: "Address10"},
		{Index: 11, Address: "Address11"},
		{Index: 12, Address: "Address12"},
		{Index: 13, Address: "Address13"},
	}
}

func destinations(t testing.TB) []string {
	t.Helper()

	return []string{"Dest1", "Dest2", "Dest3"}
}

func mockHTTPGetter(_ string) *http.Response {
	buf, err := os.ReadFile("./test_data/test_body.json")

	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	raw := "HTTP/1.0 200 OK\r\n" +
		"Connection: close\r\n" +
		"\r\n" +
		string(buf) + ""

	resp, err := http.ReadResponse(bufio.NewReader(strings.NewReader(raw)), &http.Request{Method: "GET"})

	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	return resp
}

func TestSplitOrigins(t *testing.T) {
	t.Run("split origins returns a chunked array", func(t *testing.T) {
		r := Request{BatchSize: 5, Origins: origins(t)}

		want := 3

		r.splitOrigins()
		got := len(r.BatchesChunks)

		if got != want {
			t.Errorf("got %v want %v", got, want)
		}
	})
}

func TestEnqueueBatchRequests(t *testing.T) {
	t.Run("fills up a channel with Batch objects", func(t *testing.T) {
		r := Request{BatchSize: 5, Origins: origins(t), Destinations: destinations(t)}

		r.splitOrigins()

		c := make(chan Batch, len(r.BatchesChunks))
		r.enqueueBatchRequests(c)

		want := []Batch{
			{Offset: 11, Limit: 13, Url: "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Dest1%7CDest2%7CDest3&origins=%7CAddress11%7C%7CAddress12%7C%7CAddress13%7C&key=&language=fr", Rows: Rows(nil), HTTPStatus: ""},
			{Offset: 1, Limit: 5, Url: "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Dest1%7CDest2%7CDest3&origins=%7CAddress1%7C%7CAddress2%7C%7CAddress3%7C%7CAddress4%7C%7CAddress5%7C&key=&language=fr", Rows: Rows(nil), HTTPStatus: ""},
			{Offset: 6, Limit: 10, Url: "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Dest1%7CDest2%7CDest3&origins=%7CAddress6%7C%7CAddress7%7C%7CAddress8%7C%7CAddress9%7C%7CAddress10%7C&key=&language=fr", Rows: Rows(nil), HTTPStatus: ""},
		}

		var got []Batch
		for i := 0; i < len(r.BatchesChunks); i++ {
			got = append(got, <-c)
		}

		if len(want) != len(got) {
			t.Errorf("want %v  got %v", len(want), len(got))
		}
	})
}

func TestExecRequests(t *testing.T) {
	t.Run("responses are unmarshalled into batches structs", func(t *testing.T) {
		r := Request{
			BatchSize:    5,
			Origins:      []input.Origin{{Index: 1, Address: "Address1"}},
			Destinations: destinations(t),
		}

		r.splitOrigins()

		c := make(chan Batch, len(r.BatchesChunks))
		r.enqueueBatchRequests(c)

		r.execRequests(mockHTTPGetter, c)

		want := []Batch{
			{Offset: 1, Limit: 1, Url: "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=Dest1%7CDest2%7CDest3&origins=%7CAddress1%7C&key=&language=fr",
				Rows: Rows{map[string][]Element{"elements": {
					Element{Distance: map[string]interface{}{"text": "33,6 km", "value": 33626.0}, Duration: map[string]interface{}{"text": "24 minutes", "value": 1439.0}, Status: "OK"},
					Element{Distance: map[string]interface{}{"text": "53,6 km", "value": 53582.0}, Duration: map[string]interface{}{"text": "38 minutes", "value": 2304.0}, Status: "OK"},
					Element{Distance: map[string]interface{}{"text": "29,4 km", "value": 29360.0}, Duration: map[string]interface{}{"text": "22 minutes", "value": 1348.0}, Status: "OK"},
				},
				},
				},
				HTTPStatus: "200 OK"},
		}

		var got []Batch
		for i := 0; i < len(r.BatchesChunks); i++ {
			got = append(got, <-c)
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("\ngot  %#v\nwant %#v", got, want)
		}
	})
}

func BenchmarkSplitOrigins(b *testing.B) {
	r := Request{BatchSize: 3, Origins: origins(b)}

	for n := 0; n < b.N; n++ {
		r.splitOrigins()
	}
}

func BenchmarkEnqueueBatchRequests(b *testing.B) {
	r := Request{BatchSize: 5, Origins: origins(b), Destinations: destinations(b)}

	r.splitOrigins()

	c := make(chan Batch, len(r.BatchesChunks))

	for n := 0; n < b.N; n++ {
		r.enqueueBatchRequests(c)
	}
}
