package request

import (
	"distance-matrix/internal/input"
	"fmt"
	"github.com/rs/zerolog/log"
	"net/http"
	"net/url"
)

const apiUrl = "https://maps.googleapis.com/maps/api/distancematrix/json"

type Request struct {
	APIKey        string
	BatchesChunks [][]input.Origin
	BatchSize     int
	Destinations  []string
	Origins       []input.Origin
	Results       []Batch
}

type Batch struct {
	Offset     int
	Limit      int
	Url        string
	Rows       Rows
	HTTPStatus string
}

type Rows []map[string][]Element

type Element struct {
	Distance map[string]interface{} `json:"distance"`
	Duration map[string]interface{} `json:"duration"`
	Status   string                 `json:"status"`
}

type httpGetter func(string) *http.Response

func get(url string) *http.Response {
	response, err := http.Get(url)
	if err != nil {
		log.Fatal().Err(err)
	}

	return response
}

func (r *Request) splitOrigins() {
	var chunks [][]input.Origin
	for i := 0; i < len(r.Origins); i += r.BatchSize {
		end := i + r.BatchSize

		if end > len(r.Origins) {
			end = len(r.Origins)
		}

		chunks = append(chunks, r.Origins[i:end])
	}

	r.BatchesChunks = chunks
}

func (r *Request) enqueueBatchRequests(out chan Batch) {
	// Declare request URL with destinations
	requestDestParams := url.QueryEscape(fmt.Sprintf(
		"%s|%s|%s",
		r.Destinations[0], r.Destinations[1], r.Destinations[2],
	))

	// Loop over batches to generate URL queries in go routines
	for _, batch := range r.BatchesChunks {
		go func(localBatch []input.Origin) {
			batchUrl := fmt.Sprintf("%s?destinations=%s&origins=", apiUrl, requestDestParams)

			var originsParameters string
			for _, location := range localBatch {
				originsParameters = fmt.Sprintf("%s|%s|", originsParameters, location.Address)
			}

			batchUrl = fmt.Sprintf(
				"%s%s&key=%s&language=fr",
				batchUrl,
				url.QueryEscape(originsParameters),
				r.APIKey,
			)
			out <- Batch{Url: batchUrl, Offset: localBatch[0].Index, Limit: localBatch[len(localBatch)-1].Index}
		}(batch)
	}
}

func (r *Request) execRequests(get httpGetter, batchesChan chan Batch) {
	for i := 0; i < len(r.BatchesChunks); i++ {
		go func(batch Batch) {
			response := get(batch.Url)

			UnmarshalBatch(response, &batch)

			batchesChan <- batch
		}(<-batchesChan)
	}
}

func (r *Request) Exec(csv input.CSV) []Batch {
	r.Origins = csv.Origins
	r.Destinations = csv.Destinations
	r.splitOrigins()

	c := make(chan Batch, len(r.BatchesChunks))
	r.enqueueBatchRequests(c)
	r.execRequests(get, c)

	var results []Batch
	for i := 0; i < len(r.BatchesChunks); i++ {
		results = append(results, <-c)
	}

	r.Results = results

	return r.Results
}
