package request

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io"
	"net/http"
)

func UnmarshalBatch(response *http.Response, batch *Batch) {
	// Read body batchresponse (json string)
	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Fatal().Err(err)
	}

	log.Info().Str("URL", batch.Url).Str("Status", response.Status).Send()

	var bodyJsonMap map[string]interface{}
	// Partially unmarshall json string (stop at rows)
	err = json.Unmarshal(body, &bodyJsonMap)
	if err != nil {
		log.Fatal().Err(err)
	}

	// Extract rows from json batchresponse
	rowsStr, err := json.Marshal(bodyJsonMap["rows"])
	if err != nil {
		log.Fatal().Err(err)
	}

	// Eventually unmarshal rows array into custom type, and store into Batch struct
	var rows Rows

	err = json.Unmarshal(rowsStr, &rows)
	if err != nil {
		log.Fatal().Err(err)
	}

	batch.Rows = rows
	batch.HTTPStatus = response.Status
}
