package input

import (
	"encoding/csv"
	"log"
	"os"
	"strings"
)

type CSV struct {
	FileName               string
	Records                [][]string
	OriginsLineOffset      int
	OriginsAddressOffset   int
	DestinationsLineOffset int
	DestinationsColOffset  int
	Origins                []Origin
	Destinations           []string
}

type Origin struct {
	Index   int
	Address string
}

func (csvInput *CSV) ExtractRecords() {
	// Extract input CSV file as Go string
	fileBytes, err := os.ReadFile(csvInput.FileName)
	if err != nil {
		log.Fatalf("Error reading input file: %s", err)
	}

	// Instantiate standard CSV reader from extracted string
	fileString := string(fileBytes)
	csvReader := csv.NewReader(strings.NewReader(fileString))
	csvReader.Comma = ','

	// Extract records
	csvInput.Records, err = csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
}

func (csvInput *CSV) ExtractDestinations() {
	csvInput.Destinations = []string{
		csvInput.Records[csvInput.DestinationsLineOffset][csvInput.DestinationsColOffset],
		csvInput.Records[csvInput.DestinationsLineOffset][csvInput.DestinationsColOffset+2],
		csvInput.Records[csvInput.DestinationsLineOffset][csvInput.DestinationsColOffset+4],
	}
}

func (csvInput *CSV) ExtractOrigins() {
	var tmpOrigins []Origin
	for key, address := range csvInput.Records {
		// Full address is stored in the fifth index of the record line
		origin := Origin{Index: key, Address: address[csvInput.OriginsAddressOffset]}
		tmpOrigins = append(tmpOrigins, origin)
	}

	csvInput.Origins = tmpOrigins[csvInput.OriginsLineOffset:]
}

func (csvInput *CSV) PrepareData() {
	csvInput.ExtractRecords()
	csvInput.ExtractDestinations()
	csvInput.ExtractOrigins()
}
