package input

import (
	"encoding/csv"
	"os"
	"reflect"
	"testing"
)

func newFile(t testing.TB, testName string) *os.File {
	t.Helper()

	f, err := os.CreateTemp("/tmp", testName)
	if err != nil {
		t.Fatalf("TempFile %s: %s", testName, err)
	}

	return f
}

func records(t testing.TB) *[][]string {
	t.Helper()

	// Declare test CSV structure
	return &[][]string{
		{"", "destinationOne", "", "destinationTwo", "", "destinationThree", ""},
		{"OriginOne", "", "", "", "", "", ""},
		{"OriginTwo", "", "", "", "", "", ""},
		{"OriginThree", "", "", "", "", "", ""},
	}
}

func deferCloseFile(t testing.TB, f *os.File) (func(), func()) {
	t.Helper()

	removeFile := func() {
		err := os.Remove(f.Name())
		if err != nil {
			t.Fatalf("remove test file %s - %s", f.Name(), err)
		}
	}

	closeFile := func() {
		err := f.Close()
		if err != nil {
			t.Fatalf("close test file %s - %s", f.Name(), err)
		}
	}

	return removeFile, closeFile
}

func createTempCSV(t testing.TB, testName string) *os.File {
	f := newFile(t, testName)

	// Declare test CSV structure
	records := *records(t)

	// Write CSV structure to temp test file
	w := csv.NewWriter(f)
	err := w.WriteAll(records)
	if err != nil {
		t.Fatalf("error writing csv: %s", err)
	}

	return f
}

func TestExtractRecords(t *testing.T) {
	f := createTempCSV(t, "_Go_TestExtractRecords")
	removeFile, closeFile := deferCloseFile(t, f)
	defer removeFile()
	defer closeFile()

	t.Run("test records extraction from CSV file", func(t *testing.T) {
		csvInput := CSV{
			FileName: f.Name(),
		}

		csvInput.ExtractRecords()

		want := records(t)
		got := &csvInput.Records

		if !reflect.DeepEqual(&got, &want) {
			t.Errorf("want %v got %v", &want, &got)
		}
	})
}

func TestExtractDestinations(t *testing.T) {
	f := createTempCSV(t, "_Go_TestExtractRecords")
	removeFile, closeFile := deferCloseFile(t, f)
	defer removeFile()
	defer closeFile()

	t.Run("test destinations extraction from CSV file", func(t *testing.T) {
		csvInput := CSV{
			FileName:               f.Name(),
			DestinationsLineOffset: 0,
			DestinationsColOffset:  1,
		}

		csvInput.Records = *records(t)
		csvInput.ExtractDestinations()

		want := []string{"destinationOne", "destinationTwo", "destinationThree"}

		got := csvInput.Destinations

		if !reflect.DeepEqual(got, want) {
			t.Errorf("want %v got %v", want, got)
		}
	})
}

func TestExtractOrigins(t *testing.T) {
	f := createTempCSV(t, "_Go_TestExtractRecords")
	removeFile, closeFile := deferCloseFile(t, f)
	defer removeFile()
	defer closeFile()

	t.Run("test origins extraction from CSV file", func(t *testing.T) {
		csvInput := CSV{
			FileName:             f.Name(),
			OriginsLineOffset:    1,
			OriginsAddressOffset: 0,
		}

		csvInput.Records = *records(t)
		csvInput.ExtractOrigins()

		want := []Origin{
			{Index: 1, Address: "OriginOne"},
			{Index: 2, Address: "OriginTwo"},
			{Index: 3, Address: "OriginThree"},
		}

		got := csvInput.Origins

		if !reflect.DeepEqual(got, want) {
			t.Errorf("want %v got %v", want, got)
		}
	})
}

func BenchmarkExtractRecords(b *testing.B) {
	f := createTempCSV(b, "_Go_TestExtractRecords")
	removeFile, closeFile := deferCloseFile(b, f)
	defer removeFile()
	defer closeFile()

	csvInput := CSV{
		FileName: f.Name(),
	}

	for n := 0; n < b.N; n++ {
		csvInput.ExtractRecords()
	}
}

func BenchmarkExtractDestinations(b *testing.B) {
	f := createTempCSV(b, "_Go_TestExtractRecords")
	removeFile, closeFile := deferCloseFile(b, f)
	defer removeFile()
	defer closeFile()

	csvInput := CSV{
		FileName:               f.Name(),
		DestinationsLineOffset: 0,
		DestinationsColOffset:  1,
	}

	csvInput.Records = *records(b)

	for n := 0; n < b.N; n++ {
		csvInput.ExtractDestinations()
	}
}

func BenchmarkExtractOrigins(b *testing.B) {
	f := createTempCSV(b, "_Go_TestExtractRecords")
	removeFile, closeFile := deferCloseFile(b, f)
	defer removeFile()
	defer closeFile()

	csvInput := CSV{
		FileName:               f.Name(),
		DestinationsLineOffset: 0,
		DestinationsColOffset:  1,
	}

	csvInput.Records = *records(b)

	for n := 0; n < b.N; n++ {
		csvInput.ExtractDestinations()
	}
}